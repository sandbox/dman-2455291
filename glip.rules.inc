<?php
/**
 * @file
 * Rules declarations for glip.module.
 *
 * Provides 'Send glip message' action.
 */

/**
 * Implement hook_rules_action_info().
 */
function glip_rules_action_info() {
  return array(
    'glip_rules_action_send_message' => array(
      'label' => t('Send glip message'),
      'group' => t('Messaging'),
      'parameter' => array(
        'body' => array(
          'type' => 'text',
          'label' => t('Message body'),
          'description' => t('Enter message body. May use glip-style markdown.'),
        ),
        'activity' => array(
          'type' => 'text',
          'label' => t('"Activity" label'),
          'description' => t('If left empty, it will identify the rule being executed.'),
          'optional' => TRUE,
          'default value' => '',
          'allow null' => TRUE,
        ),
        'link' => array(
          'type' => 'text',
          'label' => t('Link'),
          'description' => t('OPTIONAL link to make the message clickable'),
          'default value' => '',
          'optional' => TRUE,
          'allow null' => TRUE,
          'default mode' => 'selector',
        ),
        'endpoint' => array(
          'type' => 'text',
          'label' => t('Glip API endpoint'),
          'description' => t('OPTIONAL. The Glip integration endpoint that defines where the message goes. If not set, will use the site default channel.'),
          'optional' => TRUE,
          'default value' => '',
          'allow null' => TRUE,
          'default mode' => 'input',
        ),
      ),
    ),
  );
}

/**
 * Sends a glip message with the given values.
 *
 * An action callback.
 *
 * @param string $body
 *   Text.
 *
 * @param string $activity
 *   super-title.
 *
 * @param string $link
 *   Optional URL to link the message to.
 *   (Markdown works also)
 *
 * @param string $endpoint
 *   Optional system URL designating the conversation to send to.
 *
 * @param array $params
 *   Array of currently avaiable parameters.
 *
 * @param RulesState $state
 *   Something about the action.
 *
 * @param RulesAction $action
 *   Currently running rule context.
 *
 * @param string $op
 *   Probably 'execute'
 */
function glip_rules_action_send_message($body, $activity, $link, $endpoint, $params, $state, $action, $op) {
  if (empty($activity)) {
    // Construct a label for this message that explains where the message came
    // from. This will help avoid frustration when it gets noisy.
    // $action is about what's happening now.
    // $action->parent contains info about what's being run!
    // $action->parent->label is what I want.
    $current_executing_rule = $action->parentElement();
    $activity = $current_executing_rule->label;
    if (!empty($current_executing_rule->tags)) {
      $strings = array(
        '@tags' => implode(',', $current_executing_rule->tags),
        '@activity' => $activity,
      );
      $activity = t('[@tags] @activity', $strings);
    }
  }

  $message = array(
    'body' => $body,
    'activity' => $activity,
    'link' => $link,
  );
  glip($message, $endpoint);
}
